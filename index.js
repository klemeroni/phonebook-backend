const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require('cors')
app.use(express.static('build'))


app.use(bodyParser.json());
app.use(cors())

let persons = [
  {
    name: "Arto Hallas",
    number: "040-123456",
    id: 1,
  },
  {
    name: "Martti Tienari",
    number: "040-123456",
    id: 2,
  },
  {
    name: "Arto Järvinen",
    number: "040-123456",
    id: 3,
  },
];

app.get("/api/persons", (req, res) => {
  res.json(persons);
});

app.get("/api/persons/:id", (req, res) => {
  const id = Number(req.params.id);
  const person = persons.find((person) => person.id === id);

  if (person) {
    res.json(person);
  } else {
    res.status(404).end();
  }
});

app.delete("/api/persons/:id", (request, response) => {
  const id = Number(request.params.id);
  persons = persons.filter((person) => person.id !== id);

  response.status(204).end();
});

app.post("/api/persons", (request, response) => {
  var maximum = 100000;
  var minimum = 0;
  const body = request.body;

  if (body.name === undefined || body.number === undefined || persons.filter(person => person.name === body.name)) {
    return response.status(400).json({ error: "henkilö on jo olemassa tai puuttuu tietoja" });
  }

  const person = {
    name: body.name,
    number: body.number,
    id: Math.floor(Math.random() * (maximum - minimum + 1)) + minimum,
  };

  persons = persons.concat(person);

  response.json(person);
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
